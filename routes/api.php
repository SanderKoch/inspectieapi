<?php

use App\Http\Controllers\FileController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('file/upload', [FileController::class, 'fileUpload'])
    ->middleware('auth:api');

Route::post('file/{id}', [FileController::class, 'saveImageFromUser'])
    ->middleware('auth:api');

Route::get('file/thumbnail', [FileController::class, 'fetchAllThumbnailsFromUser'])
    ->middleware('auth:api');

Route::get('file/{id}', [FileController::class, 'fetchImageFromUser'])
    ->middleware('auth:api');

Route::get('sync', [FileController::class, 'syncData'])
    ->middleware('auth:api');

Route::post('user/login', [UserController::class, 'login']);
