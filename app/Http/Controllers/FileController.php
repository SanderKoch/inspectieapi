<?php

namespace App\Http\Controllers;

use App\Models\Blueprint;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use ZipStream\ZipStream;

class FileController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function fileUpload(Request $request)
    {
        $file = $request->file('file');

        $this->saveImage($request, $file);

        return $request->all();
    }

    function saveImage($request, $file, $fileId = null)
    {
        try {
            //Save Full image
            $file->storeAs("users/" . $request->user()["id"] . "/full/", $file->hashName());

            //Create instance of full image and resize
            $image = Image::make($file);
            $image->resize(null, 75, function ($constraint) {
                //Preserve aspect ratio and prevent upsizing
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image->stream($file->extension(), 100);

            //Save resized image to thumbnail folder
            Storage::disk("images")->put($request->user()["id"] . "/thumbnails/" . $file->hashName(), $image);
        } catch (\Exception $e) {
            return $e;
        }

        if ($fileId === null) {
            //Create blueprint model
            Blueprint::create([
                "dump" => $file->hashName(),
                "user_id" => $request->user()["id"]
            ]);
        } else {
            //Update existing blueprint model
            Blueprint::where("id", $fileId)
                ->update(["dump" => $file->hashname()]);
        }


        return null;
    }

    function saveImageFromUser(Request $request, $id)
    {
        $user = $request->user();
        $file = $request->file('file');

        $imageLocation = Blueprint::select("dump")
            ->where("id", $id)
            ->first();

        File::delete($user["id"] . "/full/" . $imageLocation->dump);
        File::delete($user["id"] . "/thumbnails/" . $imageLocation->dump);

        $this->saveImage($request, $file, $id);

        return print_r($file);
    }

    function fetchImageFromUser(Request $request, $id)
    {
        $user = $request->user();
        $imageLocation = Blueprint::select("dump")
            ->where("id", $id)
            ->first();

        return Storage::disk("images")->response($user["id"] . "/full/" . $imageLocation->dump);
    }

    //TODO
    function fetchThumbnailFromUser(Request $request)
    {
        $user = $request->user();
        $id = $user["id"];
        $imageLocations = Blueprint::select("dump")
            ->where("user_id", $id)
            ->first();

        return Storage::disk("images")->response($id . "/thumbnails/" . $imageLocations->dump);
    }

    //TODO
    function syncData(Request $request)
    {
        $user = $request->user();
        $id = $user["id"];
        return json_encode(Blueprint::where("user_id", $id)->get());
    }

    //TODO
    function fetchAllThumbnailsFromUser(Request $request)
    {
        $user = $request->user();
        $id = $user["id"];

        $imageLocations = Blueprint::select("dump")
            ->where("user_id", $id)
            ->get();

        return response()->stream(function () use ($imageLocations, $user) {
            $zip = new ZipStream("test.zip");

            foreach ($imageLocations as $imageLocation) {
                $file = Storage::disk("images")->read($user["id"] . "/thumbnails/" . $imageLocation->dump);
                $zip->addFile($imageLocation->dump, $file);
            }

            $zip->finish();
        });
    }
}
